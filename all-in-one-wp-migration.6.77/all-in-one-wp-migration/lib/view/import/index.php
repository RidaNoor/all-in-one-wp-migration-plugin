<?php
/**
 * Copyright (C) 2014-2017 ServMask Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ███████╗███████╗██████╗ ██╗   ██╗███╗   ███╗ █████╗ ███████╗██╗  ██╗
 * ██╔════╝██╔════╝██╔══██╗██║   ██║████╗ ████║██╔══██╗██╔════╝██║ ██╔╝
 * ███████╗█████╗  ██████╔╝██║   ██║██╔████╔██║███████║███████╗█████╔╝
 * ╚════██║██╔══╝  ██╔══██╗╚██╗ ██╔╝██║╚██╔╝██║██╔══██║╚════██║██╔═██╗
 * ███████║███████╗██║  ██║ ╚████╔╝ ██║ ╚═╝ ██║██║  ██║███████║██║  ██╗
 * ╚══════╝╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚═╝     ╚═╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝
 */
?>

<div class="ai1wm-container">
	<div class="ai1wm-row">
		<div class="ai1wm-left">
			<div class="ai1wm-holder">
				<h1><i class="ai1wm-icon-publish"></i> <?php _e( 'Import Site', AI1WM_PLUGIN_NAME ); ?></h1>

				<?php include AI1WM_TEMPLATES_PATH . '/common/report-problem.php'; ?>
				<form id="ridanoor-url-import">
<section>
<div class="ai1wm-field">
			<input placeholder="Enter .wpress file url..." type="url" id="ridanoor-url-import-field" class="ai1wm-report-email" required="required">
		</div>
		<div class="ai1wm-buttons">
				<button type="submit" id="ridanoor-url-import-submit" class="ai1wm-button-blue">
					Proceede			</button>
				<a href="#" id="ai1wm-report-cancel" class="ai1wm-report-cancel"  onclick="hideOverlay();">Cancel</a>
			</div>
		</section>
		

					<input type="hidden" name="ai1wm_manual_import" value="1" />

</form>

				<form action="" method="post" id="ai1wm-import-form" class="ai1wm-clear" enctype="multipart/form-data">

					<p>
						<?php _e( 'Use the box below to upload a wpress file.', AI1WM_PLUGIN_NAME ); ?><br />
					</p>

					<?php include AI1WM_TEMPLATES_PATH . '/import/import-buttons.php'; ?>

					<?php do_action( 'ai1wm_import_left_end' ); ?>

					<input type="hidden" name="ai1wm_manual_import" value="1" />

				</form>
			</div>
		</div>
		<div class="ai1wm-right">
			<div class="ai1wm-sidebar">
				<div class="ai1wm-segment">
					<?php if ( ! AI1WM_DEBUG ) : ?>
						<?php include AI1WM_TEMPLATES_PATH . '/common/share-buttons.php'; ?>
					<?php endif; ?>

					<h2><?php _e( 'Leave Feedback', AI1WM_PLUGIN_NAME ); ?></h2>

					<?php include AI1WM_TEMPLATES_PATH . '/common/leave-feedback.php'; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="display:none;" id="ridanoor-overlay-url">

<div class="ai1wm-overlay"></div>
<div class="ai1wm-modal-container"></div>
<div class="ai1wm-overlay" style="display: block;"></div>
<div class="ai1wm-modal-container" style="display: block;"><div>

</div>
</div>
</div>
<script>
	function hideOverlay(){
	document.getElementById('ridanoor-overlay-url').style.display = "none";
	}
	
	function showOverlay(){
	document.getElementById('ridanoor-overlay-url').style.display = "block";
	}
	
	
	
	function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}


	$(document).ready(function() {
	
	$('#ridanoor-url-import').submit(function(event) {
	
        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
	
	var params= {
            'priority'              : 7,
            'secret_key'             :ai1wm_import.secret_key ,
            'archive'    : makeid()
        };
        
        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : ai1wm_import.ajax.url, // the url where we want to POST
            data        : params, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
                        encode          : true
        })
            // using the done promise callback
            .done(function(data) {

                // log data to the console so we can see
                console.log(data); 

                // here we will handle errors and validation messages
            });
	});

	});
	
</script>